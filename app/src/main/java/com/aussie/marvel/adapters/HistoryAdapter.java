package com.aussie.marvel.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import java.util.List;

/**
 * Created by aussie on 11/3/16.
 */

public class HistoryAdapter extends ArrayAdapter<String> {
    private final LayoutInflater mInflater;

    public HistoryAdapter(Context context, List<String> data) {
        super(context, android.R.layout.simple_spinner_item, data);
        this.mInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = mInflater.inflate(android.R.layout.simple_list_item_1, null, true);

        String item = getItem(position);
        ((TextView) view.findViewById(android.R.id.text1)).setText(item);

        return view;
    }
}
