/**
 * Created by aussie on 10/11/15.
 */

//package com.aussie.marvel.providers;
//
//import android.content.ContentProvider;
//import android.content.ContentValues;
//import android.content.UriMatcher;
//import android.database.Cursor;
//import android.database.sqlite.SQLiteDatabase;
//import android.net.Uri;
//import android.util.Log;
//
//import com.aussie.marvel.utils.DBHelper;
//
//import java.util.List;
//
//public class DBContentProvider extends ContentProvider {
//    final String LOG_TAG = "pprDB";
//
//    final static String DB_NAME = "pprDB.db";
//    final static int DB_VERSION = 1;
//
//    static final String HIST_TABLE = "history";
//
//    // Uri authority
//    public static final String AUTHORITY = "com.aussie.marvel.provider.DBData";
//
//    // Uri  path
//    public static final String HIST_PATH = "history";
//    public static final String HIST_PATH_ROW = "history/row";
//    public static final String HIST_PATH_INSERT = "history/insert";
//    public static final String HIST_PATH_DELETE = "history/delete";
//    public static final String HIST_PATH_UPDATE = "history/update";
//    public static final String HIST_PATH_LAST5 = "history/last5";
//
//    // Общий Uri
//    public static final Uri TRIP_URI = Uri.parse("content://" + AUTHORITY + "/" + HIST_PATH);
//
//    // Типы данных
//    // набор строк
//    static final String TRIP_CONTENT_TYPE = "vnd.android.cursor.dir/vnd." + AUTHORITY + "." + HIST_PATH;
//    // одна строка
//    static final String TRIP_CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd." + AUTHORITY + "." + HIST_PATH;
//
//    //// UriMatcher
//    // общий Uri
//    static final int URI_TRIP_ALLDATA = 0; // get all records for the trip
//    static final int URI_TRIP = 1; // get all records for the trip
//    static final int URI_TRIP_ROW = 2; // get all records for the trip
//    static final int URI_TRIP_NEWDATA = 3;
//    static final int URI_TRIP_INSERT = 4;
//    static final int URI_TRIP_UPDATE = 5;
//    static final int URI_TRIP_DELETE = 6;
//
//
//    // описание и создание UriMatcher
//    private static final UriMatcher uriMatcher;
//
//    static {
//        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
//        uriMatcher.addURI(AUTHORITY, TRIP_PATH, URI_TRIP_ALLDATA);
//        uriMatcher.addURI(AUTHORITY, TRIP_PATH + "/#", URI_TRIP);
//        uriMatcher.addURI(AUTHORITY, TRIP_PATH_ROW + "/#", URI_TRIP_ROW);
//        uriMatcher.addURI(AUTHORITY, TRIP_PATH_NEWDATA + "/#", URI_TRIP_NEWDATA);
//        uriMatcher.addURI(AUTHORITY, TRIP_PATH_INSERT + "/#", URI_TRIP_INSERT);
//        uriMatcher.addURI(AUTHORITY, TRIP_PATH_DELETE + "/#/#", URI_TRIP_DELETE);
//        uriMatcher.addURI(AUTHORITY, TRIP_PATH_UPDATE + "/#/#", URI_TRIP_UPDATE);
//    }
//
//    DBHelper dbHelper;
//    SQLiteDatabase db;
//
//
//    public DBContentProvider() {
//
//    }
//
//    @Override
//    public boolean onCreate() {
//        dbHelper = new DBHelper(getContext());
//        db = dbHelper.getWritableDatabase();
//
//        return true;
//    }
//
//    @Override
//    public String getType(Uri uri) {
//        Log.d(LOG_TAG, "getType, " + uri.toString());
//
//        switch (uriMatcher.match(uri)) {
//            case URI_TRIP:
//            case URI_TRIP_NEWDATA:
//                return TRIP_CONTENT_TYPE;
//            case URI_TRIP_ROW:
//                return TRIP_CONTENT_ITEM_TYPE;
//        }
//
//        return null;
//    }
//
//    @Override
//    public int delete(Uri uri, String selection, String[] selectionArgs) {
//        int cnt = 0;
//
//        switch (uriMatcher.match(uri)) {
//            case URI_TRIP_ALLDATA:
//                // only if has "where" condition
//                if(selection.length() > 0) {
//                    cnt = db.delete(HIST_TABLE, selection, selectionArgs);
//                }
//                break;
//            case URI_TRIP_DELETE:
//                List<String> params = uri.getPathSegments();
//
//                if (params.size() == 3) {
//                    // if only one parameter then use pprid filter
//                    selection = "pprid = " + params.get(params.size() - 1);
//                } else if (params.size() == 4) {
//                    // if parameters more than one, use pprid and record id
//                    selection = "pprid = " + params.get(params.size() - 2) + " and id = " + params.get(params.size() - 1);
//                }
//
//                cnt = db.delete(HIST_TABLE, selection, selectionArgs);
//                break;
//        }
//
//        return cnt;
//    }
//
//    @Override
//    public Uri insert(Uri uri, ContentValues values) {
//        Log.d(LOG_TAG, "Insert new data.");
//        Uri resultUri = null;
//
//        switch (uriMatcher.match(uri)) {
//            case URI_TRIP_INSERT:
//                values.put("pprid", Integer.parseInt(uri.getLastPathSegment()));
//                values.put("status", 0);
//                long rowID = db.insert(HIST_TABLE, null, values);
//                resultUri = Uri.parse("content://" + AUTHORITY + "/" + TRIP_PATH_ROW + "/" + rowID);
//                break;
//        }
//
//        getContext().getContentResolver().notifyChange(resultUri, null);
//        return resultUri;
//    }
//
//    @Override
//    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
//        Log.d(LOG_TAG, "query, " + uri.toString());
//        Cursor cursor = null;
//
//        // проверяем Uri
//        switch (uriMatcher.match(uri)) {
//            case URI_TRIP: // общий Uri
//                Log.d(LOG_TAG, "URI_TRIP");
//
//                selection = "pprid = " + uri.getLastPathSegment();
//                cursor = db.query(HIST_TABLE, projection, selection, selectionArgs, null, null, sortOrder);
//                // просим ContentResolver уведомлять этот курсор об изменениях данных в CONTACT_CONTENT_URI
//                cursor.setNotificationUri(getContext().getContentResolver(), TRIP_URI);
//                break;
//            case URI_TRIP_NEWDATA:
//                selection = "pprid = " + uri.getLastPathSegment() + " and status = 0";
//                cursor = db.query(HIST_TABLE, projection, selection, selectionArgs, null, null, sortOrder);
//                cursor.setNotificationUri(getContext().getContentResolver(), TRIP_URI);
//                break;
//            case URI_TRIP_ROW:
//                selection = "id = " + uri.getLastPathSegment();
//                cursor = db.query(HIST_TABLE, projection, selection, selectionArgs, null, null, sortOrder);
//                cursor.setNotificationUri(getContext().getContentResolver(), TRIP_URI);
//            default:
//                throw new IllegalArgumentException("Wrong URI: " + uri);
//        }
//
//        return cursor;
//    }
//
//    @Override
//    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
//        int cnt = 0;
//
//        switch (uriMatcher.match(uri)) {
//            case URI_TRIP:
//                break;
//            case URI_TRIP_UPDATE:
//                List<String> params = uri.getPathSegments();
//
//                if (params.size() == 3) {
//                    // if only one parameter then use pprid filter
//                    selection = "pprid = " + params.get(params.size() - 1);
//                } else if (params.size() == 4) {
//                    // if parameters more than one, use pprid and record id
//                    selection = "pprid = " + params.get(params.size() - 2) + " and id = " + params.get(params.size() - 1);
//                }
//
//                cnt = db.update(HIST_TABLE, values, selection, selectionArgs);
//                break;
//        }
//
//
//        getContext().getContentResolver().notifyChange(uri, null);
//        return cnt;
//    }
//}
