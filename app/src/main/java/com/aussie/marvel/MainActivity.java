package com.aussie.marvel;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.aussie.marvel.adapters.HistoryAdapter;
import com.aussie.marvel.api.DataFactory;
import com.aussie.marvel.api.MarvelDataService;
import com.aussie.marvel.api.entity.Characters;
import com.aussie.marvel.utils.Const;
import com.aussie.marvel.utils.DBHelper;
import com.aussie.marvel.utils.MD5Hash;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private static final String HISTORY_TABLE = "history";
    private static final String LOG_TAG = Const.APP_LOG_TAG + ".MainActivty";

    private final MD5Hash md5hash = new MD5Hash(); // todo: need dependency injection here
    private ProgressDialog progressDialog = null;
    private DBHelper dbHelper = new DBHelper(this);

    private EditText txtCharacter;
    private TextView lblError;

    private List<String> historyData;
    private HistoryAdapter historyAdapter;

    /**
     * Initialize activity
     *
     * @param savedInstanceState savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadHistoryData();
        historyAdapter = new HistoryAdapter(this, historyData);

        txtCharacter = (EditText) findViewById(R.id.txtCharacter);
        lblError = (TextView) findViewById(R.id.lblError);
        Button btnShow = (Button) findViewById(R.id.btnShow);
        ListView lstHistory = (ListView) findViewById(R.id.lstHistory);

        assert btnShow != null;
        btnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCharacter(txtCharacter.getText().toString());
            }
        });

        assert lstHistory != null;
        lstHistory.setAdapter(historyAdapter);
        lstHistory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String character = (String) parent.getAdapter().getItem(position);
                showCharacter(character);
            }
        });
    }

    /**
     * Show data loading dialog
     */
    private void showDataLoadDlg() {
        if (progressDialog != null) {
            return;
        }

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(this.getResources().getString(R.string.data_load));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    /**
     * Hide data loading dialog
     */
    private void hideDataLoadDlg() {
        if (progressDialog == null) {
            return;
        }

        progressDialog.hide();
        progressDialog.dismiss();
        progressDialog = null;
    }

    /**
     * Load characters history from sqllite db
     */
    private void loadHistoryData() {
        List<String> result = new ArrayList<>();

        try {
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("select character_name from history group by character_name order by fd desc limit 5", null);

            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        String character_name = cursor.getString(0);

                        result.add(character_name);
                    } while (cursor.moveToNext());
                }

                cursor.close();
            }

            db.close();
        } catch (Exception e) {
            Log.e("DB Error: ", e.toString());
        }

        this.historyData = result;
    }

    /**
     * Append character into sqllite db and update our adapter
     *
     * @param character the character name
     */
    private void appendHistory(String character) {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put("fd", new Date().getTime());
            values.put("character_name", character);

            db.insert(HISTORY_TABLE, null, values);
            db.close();

            if (historyData != null && !historyData.contains(character)) {
                historyData.add(0, character);

                // need only 5 last characters, this is ugly, but works fine
                if(historyData.size() > 5) {
                    historyData.remove(5);
                }
                historyAdapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            Log.e("DB Error: ", e.toString());
        }
    }

    /**
     * Make request to the marvel's server for the character info
     *
     * @param characterName character name for inquire
     */
    private void showCharacter(final String characterName) {
        lblError.setText("");

        if (characterName == null) {
            Log.e(LOG_TAG, "Character name can not be null");
            return;
        }

        String timestamp = Long.toString(new Date().getTime());
        String hash_md5 = md5hash.calculateMd5(timestamp + Const.MARVEL_PRIVATE_KEY + Const.MARVEL_PUBLIC_KEY);

        MarvelDataService service = DataFactory.getMarvelDataService();

        showDataLoadDlg();
        Call<Characters> call = service.findCharacters(characterName, Const.MARVEL_PUBLIC_KEY, hash_md5, timestamp);
        call.enqueue(new Callback<Characters>() {
            @Override
            public void onResponse(Call<Characters> call, Response<Characters> response) {
                hideDataLoadDlg();

                Characters characters = response.body();

                if (characters == null || characters.getData().getItems().size() == 0) {
                    Log.e(LOG_TAG, getText(R.string.err_wrong_name).toString());
                    Toast.makeText(getBaseContext(), getText(R.string.err_wrong_name), Toast.LENGTH_LONG).show();
                    lblError.setText(getText(R.string.err_wrong_name));
                } else {
                    appendHistory(characterName);
                    // we always have only one item
                    for (Characters.Item item : characters.getData().getItems()) {
                        Intent intent = new Intent(getBaseContext(), ViewActivity.class);
                        intent.putExtra(Const.PARAM_THUMBNAIL, item.getThumbnail().getPortraitXlargeImgUrl());
                        intent.putExtra(Const.PARAM_NAME, item.getName());
                        intent.putExtra(Const.PARAM_DESCRIPTION, item.getDescription());

                        startActivity(intent);
                    }
                }
            }

            @Override
            public void onFailure(Call<Characters> call, Throwable t) {
                hideDataLoadDlg();

                lblError.setText(getText(R.string.err_happend));
                Toast.makeText(getBaseContext(), getText(R.string.err_happend), Toast.LENGTH_LONG).show();
                Log.e(LOG_TAG, getText(R.string.err_happend).toString(), t);
            }
        });
    }
}