package com.aussie.marvel;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.aussie.marvel.utils.Const;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class ViewActivity extends AppCompatActivity {
    final String LOG_TAG = Const.APP_LOG_TAG + ".ViewActivty";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        Intent intent = this.getIntent();

        ImageView img = (ImageView) findViewById(R.id.imgThumbnail);
        Picasso.with(this).load(intent.getStringExtra(Const.PARAM_THUMBNAIL)).into(img);

        TextView lblCharacterName = (TextView)findViewById(R.id.lblCharacterName);
        lblCharacterName.setText(intent.getStringExtra(Const.PARAM_NAME));

        TextView lblCharacterDescr = (TextView)findViewById(R.id.lblDescription);
        lblCharacterDescr.setText(intent.getStringExtra(Const.PARAM_DESCRIPTION));
    }
}
