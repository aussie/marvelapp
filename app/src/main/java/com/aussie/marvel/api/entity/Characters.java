package com.aussie.marvel.api.entity;


//import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import com.aussie.marvel.utils.Const;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by aussie on 11/2/16.
 */

//@JsonIgnoreProperties(ignoreUnknown = true)
public class Characters {
    //    @SerializedName("code")
    private Integer code;
    //    @SerializedName("status")
    private String status;

    private Data data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        @SerializedName("results")
        private List<Item> items;

        public List<Item> getItems() {
            return items;
        }

        public void setItems(List<Item> items) {
            this.items = items;
        }
    }

    public static class Item {
        private Integer id;
        private String name;
        private String description;
        private Thumbnail thumbnail;

        public Integer getId() {

            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Thumbnail getThumbnail() {
            return thumbnail;
        }

        public void setThumbnail(Thumbnail thumbnail) {
            this.thumbnail = thumbnail;
        }
    }

    public static class Thumbnail {
        private String path;
        private String extension;

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public String getExtension() {
            return extension;
        }

        public void setExtension(String extension) {
            this.extension = extension;
        }

        public String getPortraitXlargeImgUrl() {
            return new StringBuilder(path).append('/').append(Const.MARVEL_IMG_FORMAT).append('.').append(extension).toString();
        }
    }
}

//results":[{"id":1009368,"name":"Iron Man","description":"Wounded, captured and forced to build a weapon by his enemies, billionaire industrialist Tony Stark instead created an advanced suit of armor to save his life and escape captivity. Now with a new outlook on life, Tony uses his money and intelligence to make the world a safer, better place as Iron Man.","modified":"2016-09-28T12:08:19-0400","thumbnail":{"path":"http://i.annihil.us/u/prod/marvel/i/mg/9/c0/527bb7b37ff55","extension":"jpg"},"resourceURI":"http://gateway.marvel.com/v1/public/characters/1009368","comics":{"available":2075,"collectionURI":"http://gateway.marvel.com/v1/public/characters/1009368/comics","items":[{"resourceURI":"http://gateway.marvel.com/v1/public/comics/24348","name":"Adam: Legend of the Blue Marvel (Trade Paperback)"},{"resourceURI":"http://gateway.marvel.com/v1/public/comics/22461","name":"Adam: Legend of the Blue Marvel (2008) #1"},{"resourceURI":"http://gateway.marvel.com/v1/public/comics/22856","name":"Adam: Legend of the Blue Marvel (2008) #2"},{"resourceURI":"http://gateway.marvel.com/v1/public/comics/23733","name":"Adam: Legend of the Blue Marvel (2008) #5"},{"resourceURI":"http://gateway.marvel.com/v1/public/comics/30090","name":"Age of Heroes (2010) #1"},{"resourceURI":"http://gateway.marvel.com/v1/public/comics/38524","name":"Age of X: Universe (2011) #1"},{"resourceURI":"http://gateway.marvel.com/v1/public/comics/21280","name":"All-New Iron Manual (2008) #1"},{"resourceURI":"http://gateway.marvel.com/v1/public/comics/55363","name":"All-New, All-Different Avengers (2015) #10"},{"resourceURI":"http://gateway.marvel.com/v1/public/comics/55364","name":"All-New, All-Different Avengers (2015) #11"},{"resourceURI":"http://gateway.marvel.com/v1/public/comics/12653","name":"Alpha Flight (1983) #113"},{"resourceURI":"http://gateway.marvel.com/v1/public/comics/12668","name":"Alpha Flight (1983) #127"},{"resourceURI":"http://gateway.marvel.com/v1/public/comics/55311","name":"Amazing Spider-Man (2015) #13"},{"resourceURI":"http://gateway.marvel.com/v1/public/comics/55312","name":"Amazing Spider-Man (2015) #14"},{"resourceURI":"http://gateway.marvel.com/v1/public/comics/55313","name":"Amazing Spider-Man (2015) #15"},{"resourceURI":"http://gateway.marvel.com/v1/public/comics/43170","name":"Amazing Spider-Man (1999) #57"},{"resourceURI":"http://gateway.marvel.com/v1/public/comics/43171","name":"Amazing Spider-Man (1999) #58"},{"resourceURI":"http://gateway.marvel.com/v1/public/comics/6743","name":"Amazing Spider-Man (1963) #334"},{"resourceURI":"http://gateway.marvel.com/v1/public/comics/277","name":"Amazing Spider-Man (1999) #500"},{"resourceURI":"http://gateway.marvel.com/v1/public/comics/1966","name":"Amazing Spider-Man (1999) #520"},{"resourceURI":"http://gateway.marvel.com/v1/public/comics/2980","name":"Amazing Spider-Man (1999) #523"}],"returned":20},"series":{"available":483,"collectionURI":"http://gateway.marvel.com/v1/public/characters/1009368/series","items":[{"resourceURI":"http://gateway.marvel.com/v1/public/series/7524","name":"Adam: Legend of the Blue Marvel (2008)"},{"resourceURI":"http://gateway.marvel.com/v1/public/series/6079","name":"Adam: Legend of the Blue Marvel (2008)"},{"resourceURI":"http://gateway.marvel.com/v1/public/series/9790","name":"Age of Heroes (2010)"},{"resourceURI":"http://gateway.marvel.com/v1/public/series/13896","name":"Age of X: Universe (2011)"},{"resourceURI":"http://gateway.marvel.com/v1/public/series/4897","name":"All-New Iron Manual (2008)"},{"resourceURI":"http://gateway.marvel.com/v1/public/series/20443","name":"All-New, All-Different Avengers (2015 - Present)"},{"resourceURI":"http://gateway.marvel.com/v1/public/series/2116","name":"Alpha Flight (1983 - 1994)"},{"resourceURI":"http://gateway.marvel.com/v1/public/series/454","name":"Amazing Spider-Man (1999 - 2013)"},{"resourceURI":"http://gateway.marvel.com/v1/public/series/20432","name":"Amazing Spider-Man (2015 - Present)"},{"resourceURI":"http://gateway.marvel.com/v1/public/series/1987","name":"Amazing Spider-Man (1963 - 1998)"},{"resourceURI":"http://gateway.marvel.com/v1/public/series/15540","name":"Amazing Spider-Man Annual (2012)"},{"resourceURI":"http://gateway.marvel.com/v1/public/series/2984","name":"Amazing Spider-Man Annual (1964 - 2009)"},{"resourceURI":"http://gateway.marvel.com/v1/public/series/1489","name":"Amazing Spider-Man Vol. 10: New Avengers (2005)"},{"resourceURI":"http://gateway.marvel.com/v1/public/series/318","name":"Amazing Spider-Man Vol. 6 (2004)"},{"resourceURI":"http://gateway.marvel.com/v1/public/series/6056","name":"Annihilation Classic (2008)"},{"resourceURI":"http://gateway.marvel.com/v1/public/series/14818","name":"Annihilators: Earthfall (2011)"},{"resourceURI":"http://gateway.marvel.com/v1/public/series/14779","name":"Art of Marvel Studios TPB Slipcase (2011 - Present)"},{"resourceURI":"http://gateway.marvel.com/v1/public/series/9792","name":"Astonishing Spider-Man/Wolverine (2010 - 2011)"},{"resourceURI":"http://gateway.marvel.com/v1/public/series/6792","name":"Astonishing Tales (2009)"},{"resourceURI":"http://gateway.marvel.com/v1/public/series/6697","name":"Astonishing Tales: Iron Man 2020 (2009)"}],"returned":20},"stories":{"available":3000,"collectionURI":"http://gateway.marvel.com/v1/public/characters/1009368/stories","items":[{"resourceURI":"http://gateway.marvel.com/v1/public/stories/892","name":"Cover #892","type":"cover"},{"resourceURI":"http://gateway.marvel.com/v1/public/stories/960","name":"3 of ?","type":"cover"},{"resourceURI":"http://gateway.marvel.com/v1/public/stories/982","name":"Interior #982","type":"interiorStory"},{"resourceURI":"http://gateway.marvel.com/v1/public/stories/984","name":"Interior #984","type":"interiorStory"},{"resourceURI":"http://gateway.marvel.com/v1/public/stories/986","name":"Interior #986","type":"interiorStory"},{"resourceURI":"http://gateway.marvel.com/v1/public/stories/988","name":"Interior #988","type":"interiorStory"},{"resourceURI":"http://gateway.marvel.com/v1/public/stories/990","name":"Interior #990","type":"interiorStory"},{"resourceURI":"http://gateway.marvel.com/v1/public/stories/992","name":"Interior #992","type":"interiorStory"},{"resourceURI":"http://gateway.marvel.com/v1/public/stories/994","name":"Interior #994","type":"interiorStory"},{"resourceURI":"http://gateway.marvel.com/v1/public/stories/996","name":"Interior #996","type":"interiorStory"},{"resourceURI":"http://gateway.marvel.com/v1/public/stories/998","name":"Interior #998","type":"interiorStory"},{"resourceURI":"http://gateway.marvel.com/v1/public/stories/1000","name":"Interior #1000","type":"interiorStory"},{"resourceURI":"http://gateway.marvel.com/v1/public/stories/1002","