package com.aussie.marvel.api;

import com.aussie.marvel.api.entity.Characters;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by aussie on 11/2/16.
 */

public interface MarvelDataService {
    @GET("/v1/public/characters")
    Call<Characters> findCharacters(@Query("name") String name, @Query("apikey") String apikey, @Query("hash") String hash, @Query("ts") String ts);

}


