package com.aussie.marvel.utils;

/**
 * Created by aussie on 11/2/16.
 */

public class Const {
    public static final String APP_LOG_TAG = "MarvelApp";
    public static final String APP_PREF_KEY = "com.aussie.marvel.MarvelApp";

    public static final String MARVEL_PUBLIC_KEY = "329729d36be3c44e33097d1144d824f2";
    public static final String MARVEL_PRIVATE_KEY = "6192e60523148a5e606f5c8b440f30c6fb30533f";

    public static final String API_ENDPOINT = "http://gateway.marvel.com/";

    public static final String MARVEL_IMG_FORMAT = "portrait_xlarge";

    public static final String PARAM_THUMBNAIL = "thumbnail";
    public static final String PARAM_NAME = "name";
    public static final String PARAM_DESCRIPTION = "description";
}
