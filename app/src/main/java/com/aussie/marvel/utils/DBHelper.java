package com.aussie.marvel.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {
    final String LOG_TAG = Const.APP_LOG_TAG + ".db";
    final static String DB_NAME = "marvel_db.db";

    final static int DB_VERSION = 1;

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(LOG_TAG, "---- onCreate Database ----");

        db.execSQL("create table history(id integer primary key autoincrement, fd integer, character_name text);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(LOG_TAG, "---- onUpdate Database ----");
        db.execSQL("drop table history;");
        db.execSQL("create table history(id integer primary key autoincrement, fd integer, character_name text);");
    }
}
