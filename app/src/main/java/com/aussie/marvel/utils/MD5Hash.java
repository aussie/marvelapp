package com.aussie.marvel.utils;

import android.util.Log;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by aussie on 11/2/16.
 */

public class MD5Hash {

    public String calculateMd5(String source) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            Log.e(Const.APP_LOG_TAG + ".calculateMD5", "Exception while getting Digest", e);
            return null;
        }

        BigInteger bigInt = new BigInteger(1, digest.digest(source.getBytes()));
        String result = bigInt.toString(16);
        // Fill to 32 chars
        result = String.format("%32s", result).replace(' ', '0');

        return result;
    }
}
